<!DOCTYPE html>
<html lang="en">
<?php include '_head.php'; ?>

<body>
    <?php include '_header.php'; ?>
    <div class="wrapper">
        <section class="section-title about">
            <div class="section-title-overlay"></div>
            <div class="container">
                <div class="row" data-aos="fade-right" data-aos-duration="400">
                    <div class="col-12 text-center">
                        <h2 class="h2 fw-300 text-white">Quienes somos</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="gray my-5">
            <div class="container py-4">
                <div class="row justify-content-center" data-aos="fade-up" data-aos-duration="400">
                    <div class="col-12 col-md-9 col-lg-8 text-center">
                        <h3 class="h3 fw-300 primary">Creemos que...</h3>
                        <div class="left-divider mt-2 mb-3 mx-auto d-flex"></div>
                        <p class="fw-300 h5 gray">
                            Tenemos como principal objetivo que <strong>nuestros clientes perciban y manifiesten la
                                satisfacción
                                total</strong> al utilizar nuestros productos y servicios, entendiendo que su elección
                            fue
                            la correcta.
                            Para lograr este objetivo y satisfacer sus necesidades, todo lo que ofrecemos deberá estar
                            diseñado y realizado bajo estándares de <strong><span class="primary">la más alta
                                    calidad.</span></strong>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="light-bg mt-5 py-5 py-lg-0">
            <div class="container-fluid px-lg-0">
                <div class="row align-items-center text-center text-lg-left justify-content-center">
                    <div class="col-12 col-md-8 col-lg-5 offset-lg-1 col-xl-4 offset-xl-2" data-aos="fade-right"
                        data-aos-duration="400">
                        <p class="primary h4 fw-300">
                            Nuestros desarrollos y procesos deben ser dinámicos e innovadores, acompañando los avances
                            de la ciencia y tecnología <strong>a nivel global.</strong>
                        </p>
                    </div>
                    <div class="col-12 col-md-6 d-none d-lg-block" data-aos="fade-left" data-aos-duration="400">
                        <img src="images/quienes-somos-deco1.jpg" class="w-100" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="cards my-5">
            <div class="container py-4">
                <div class="row">
                    <div class="col-12 text-center mb-3">
                        <h3 class="h3 fw-300 primary">Por qué elegirnos</h3>
                        <div class="left-divider my-2 mx-auto d-flex"></div>
                    </div>
                </div>
                <div class="cards-desktop d-none d-lg-block">
                    <div class="row">
                        <div class="col-12 col-md-3 my-3" data-aos="fade-up" data-aos-duration="400">
                            <div class="slide-container">
                                <div class="overlay"></div>
                                <img src="images/about1.jpg" alt="">
                                <div class="slide-content">
                                    <h5 class="h5 fw-700 slide-heading">Servicio al cliente</h5>
                                    <div class="slide-body">
                                        <p class="fw-300 mb-0">Los clientes deben sentirse seguros y respaldados al
                                            utilizar
                                            nuestros
                                            productos, para lo cual brindamos una <strong>
                                                sólida cobertura de seguimiento, garantías y
                                                servicio de post venta.
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 my-3" data-aos="fade-up" data-aos-duration="400">
                            <div class="slide-container">
                                <div class="overlay"></div>
                                <img src="images/about2.jpg" alt="">
                                <div class="slide-content">
                                    <h5 class="h5 fw-700 slide-heading">Procesos eficientes</h5>
                                    <div class="slide-body">
                                        <p class="fw-300 mb-0">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 my-3" data-aos="fade-up" data-aos-duration="400">
                            <div class="slide-container">
                                <div class="overlay"></div>
                                <img src="images/about3.jpg" alt="">
                                <div class="slide-content">
                                    <h5 class="h5 fw-700 slide-heading">Experiencia de calidad</h5>
                                    <div class="slide-body">
                                        <p class="fw-300 mb-0">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 my-3" data-aos="fade-up" data-aos-duration="400">
                            <div class="slide-container">
                                <div class="overlay"></div>
                                <img src="images/about4.jpg" alt="">
                                <div class="slide-content">
                                    <h5 class="h5 fw-700 slide-heading">Principios éticos</h5>
                                    <div class="slide-body">
                                        <p class="fw-300 mb-0">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cards-mobile d-block d-lg-none">
                    <div class="row" data-aos="fade-up" data-aos-duration="400">
                        <div class="col-12 my-3">
                            <div class="owl-carousel owl-theme-default" id="owl-about">
                                <div class="slide-container">
                                    <div class="slide-container-mobile">
                                        <h5 class="h5 fw-700">Servicio al cliente</h5>
                                        <p class="fw-300 mb-0">Los clientes deben sentirse seguros y respaldados al
                                            utilizar
                                            nuestros
                                            productos, para lo cual brindamos una <strong>
                                                sólida cobertura de seguimiento, garantías y
                                                servicio de post venta.
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="overlay-mobile"></div>
                                    <img src="images/about1.jpg" alt="">
                                </div>
                                <div class="slide-container">
                                    <div class="slide-container-mobile">
                                        <h5 class="h5 fw-700">Procesos eficientes</h5>
                                        <p class="fw-300 mb-0">Los clientes deben sentirse seguros y respaldados al
                                            utilizar
                                            nuestros
                                            productos, para lo cual brindamos una <strong>
                                                sólida cobertura de seguimiento, garantías y
                                                servicio de post venta.
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="overlay-mobile"></div>
                                    <img src="images/about2.jpg" alt="">
                                </div>
                                <div class="slide-container">
                                    <div class="slide-container-mobile">
                                        <h5 class="h5 fw-700">Experiencia de calidad</h5>
                                        <p class="fw-300 mb-0">Los clientes deben sentirse seguros y respaldados al
                                            utilizar
                                            nuestros
                                            productos, para lo cual brindamos una <strong>
                                                sólida cobertura de seguimiento, garantías y
                                                servicio de post venta.
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="overlay-mobile"></div>
                                    <img src="images/about3.jpg" alt="">
                                </div>
                                <div class="slide-container">
                                    <div class="slide-container-mobile">
                                        <h5 class="h5 fw-700">Principios éticos</h5>
                                        <p class="fw-300 mb-0">Los clientes deben sentirse seguros y respaldados al
                                            utilizar
                                            nuestros
                                            productos, para lo cual brindamos una <strong>
                                                sólida cobertura de seguimiento, garantías y
                                                servicio de post venta.
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="overlay-mobile"></div>
                                    <img src="images/about4.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="gray bg-light py-5 py-lg-0">
            <div class="container-fluid px-lg-0">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6 d-none d-lg-block" data-aos="fade-right" data-aos-duration="400">
                        <img src="images/quienes-somos-deco2.jpg" class="w-100 join-deco" alt="">
                    </div>
                    <div class="col-12 col-lg-5 col-xl-4" data-aos="fade-left" data-aos-duration="400">
                        <h3 class="h3 fw-300 primary">Nuestro equipo</h3>
                        <div class="left-divider mt-2 mb-3"></div>
                        <img src="images/quienes-somos-deco2.jpg" class="w-100 d-block d-lg-none mb-3" alt="">
                        <p class="fw-300 h5 gray my-4">
                            Nuestros empleados y colaboradores son el recurso <strong>más valioso</strong> con el que
                            contamos. Debemos
                            respetar su dignidad, reconocer sus méritos y acompañar su responsabilidad familiar.
                            <strong>
                                Contenerlos
                                en sus empleos, procurando espacios de trabajo ordenados, limpios y seguros, valorando
                                la
                                importancia de un buen clima laboral.
                            </strong> Dar lugar a sugerencias, atender sus necesidades y ofrecer
                            un salario justo al igual que la posibilidad de crecimiento profesional.
                        </p>
                        <a href="" class="btn btn-primary mx-auto">Sumate al equipo</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include '_footer.php'; ?>
    <?php include '_scripts.php'; ?>
</body>

</html>