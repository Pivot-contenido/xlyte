$(document).ready(function () {
    $("li a[href='" + location.href.substring(location.href.lastIndexOf("/") + 1, 255) + "']").addClass("active-nav");
});


$(".nav-link").click(function () {
    $(".custom-hamburger").toggleClass("is-active");
});


document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

AOS.init();



$(document).ready(function () {
    $("#owl-home").owlCarousel({
        center: false,
        items: 3,
        loop: true,
        margin: 25,
        dots: false,
        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
        nav: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 3,
                nav: true
            },
            1024: {
                items: 4,
                nav: true
            },
        }
    });
    $("#owl-about").owlCarousel({
        center: false,
        items: 3,
        loop: true,
        margin: 25,
        dots: false,
        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
        nav: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
                nav: true
            }
        }
    });
});


