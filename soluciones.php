<!DOCTYPE html>
<html lang="en">
<?php include '_head.php'; ?>

<body>
    <?php include '_header.php'; ?>
    <section class="systems">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5 py-5 py-md-5 pt-lg-5 pb-lg-0 pl-xl-5 order-2 order-lg-1" data-aos="fade-right" data-aos-duration="500">
                    <h2 class="h3 fw-300 primary">Sistemas de backup</h2>
                    <div class="left-divider my-2"></div>
                    <p class="h5 fw-300">Comercial</p>

                    <img src="images/sistema1.png" class="systems-deco" alt="">

                    <div class="d-none d-md-block">
                        <p class="h5 fw-300">Características</p>
                        <div class="left-divider my-2"></div>
                        <p class="mb-5">Lorem ipsum dolor sit amet. At vero eos et accusam et justo duo dolores et ea
                            rebum.
                            Stet clita
                            kasd gubergren, no sea <span class="fw-700 primary">takimata sanctus est</span> Lorem ipsum
                            dolor sit amet. Lorem ipsum dolor
                            sit
                            amet, consetetur sadipscing elitr, sed dia voluptua. </p>
                        <a href="" class="btn btn-primary mx-auto">Quiero más información</a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 light-bg systems-info order-3 order-lg-2" data-aos="fade-up" data-aos-duration="600">
                    <div class="systems-content">
                        <div>
                            <p class="h5 fw-300">Datos técnicos</p>
                            <div class="left-divider my-2"></div>
                        </div>
                        <ul class="mb-0">
                            <li>
                                Compatibilidad con todos los sistemas fotovoltaicos.
                            </li>
                            <li>
                                Suministro de energía incluso en caso de apagado
                            </li>
                            <li>
                                Almacenamiento de baterías acopladas de CA
                            </li>
                            <li>
                                Célula LIFEPO4, segura y respetuosa con el medio ambiente.
                            </li>
                        </ul>
                        <div class="row mt-3 d-none d-md-flex">
                            <div class="col-6 text-center my-3">
                                <p class="fw-700 mb-0">
                                    Autonomía
                                </p>
                                <p class="primary h2 fw-300">
                                    100 <span class="h5">km</span>
                                </p>
                            </div>
                            <div class="col-6 text-center my-3">
                                <p class="fw-700 mb-0">
                                    Temperatura
                                </p>
                                <p class="primary h2 fw-300">
                                    0º<span class="h5">a </span>55º C
                                </p>
                            </div>
                            <div class="col-6 text-center my-3">
                                <p class="fw-700 mb-0">
                                    Eficiencia
                                </p>
                                <p class="primary h2 fw-300 mb-0">
                                    90<span class="h5">%</span>
                                </p>
                                <p class="text-uppercase">de carga</p>
                            </div>
                            <div class="col-6 text-center my-3">
                                <p class="fw-700 mb-0">
                                    Característica
                                </p>
                                <p class="primary h2 fw-300">
                                    50<span class="h5">min</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 px-0 order-1 order-lg-3" data-aos="fade" data-aos-duration="800">
                    <img src="images/sistema1-foto.jpg" class="w-100 systems-foto d-none d-lg-block" alt="">
                    <img src="images/sistema1-foto-mobile.jpg" class="w-100 systems-foto d-block d-lg-none" alt="">
                </div>
                <div class="col-12 d-block d-md-none order-4 mb-5 mt-3" data-aos="fade-left" data-aos-duration="600">
                    <div class="row mb-3 d-flex d-md-none">
                        <div class="col-6 text-center my-3">
                            <p class="fw-700 mb-0">
                                Autonomía
                            </p>
                            <p class="primary h2 fw-300">
                                100 <span class="h5">km</span>
                            </p>
                        </div>
                        <div class="col-6 text-center my-3">
                            <p class="fw-700 mb-0">
                                Temperatura
                            </p>
                            <p class="primary h2 fw-300">
                                0º<span class="h5">a </span>55º C
                            </p>
                        </div>
                        <div class="col-6 text-center my-3">
                            <p class="fw-700 mb-0">
                                Eficiencia
                            </p>
                            <p class="primary h2 fw-300 mb-0">
                                90<span class="h5">%</span>
                            </p>
                            <p class="text-uppercase">de carga</p>
                        </div>
                        <div class="col-6 text-center my-3">
                            <p class="fw-700 mb-0">
                                Característica
                            </p>
                            <p class="primary h2 fw-300">
                                50<span class="h5">min</span>
                            </p>
                        </div>
                    </div>
                    <p class="h5 fw-300">Características</p>
                    <div class="left-divider my-2"></div>
                    <p class="mb-5">Lorem ipsum dolor sit amet. At vero eos et accusam et justo duo dolores et ea rebum.
                        Stet clita
                        kasd gubergren, no <span class="fw-700 primary">takimata sanctus est</span> est Lorem ipsum
                        dolor sit amet. Lorem ipsum dolor sit
                        amet, consetetur sadipscing elitr, sed dia voluptua. </p>
                    <a href="" class="btn btn-primary mx-auto">Quiero más información</a>
                </div>
            </div>
        </div>
    </section>
    <section class="faq py-5">
        <div class="container">
            <div class="row" data-aos="fade-up" data-aos-duration="500">
                <div class="col-12 text-center my-4">
                    <p class="primary fw-300 mb-0">Encontrá respuestas</p>
                    <h4 class="h2 fw-300">Preguntas frecuentes</h4>
                </div>
            </div>
            <div class="row" data-aos="fade-up" data-aos-duration="600">
                <div class="col-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <button class="card-header" id="headingOne" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <p class="mb-0"><span class="fw-700">01.</span> ¿Qúe es un sistema de backup? </p>
                            </button>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
                                        vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                                        no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <button class="card-header collapsed" id="headingTwo" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                <p class="mb-0"><span class="fw-700">02.</span> ¿Cuáles son las ventajas de los sistemas
                                    de backup? </p>
                            </button>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
                                        vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                                        no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <button class="card-header collapsed" id="headingThree" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                <p class="mb-0"><span class="fw-700">03.</span> ¿Cómo funciona? </p>
                            </button>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
                                        vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                                        no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <button class="card-header collapsed" id="headingFour" type="button" data-toggle="collapse"
                                data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                <p class="mb-0"><span class="fw-700">04.</span> ¿Cuánto dura la batería de un Sistema de
                                    backup? </p>
                            </button>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
                                        vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                                        no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <button class="card-header collapsed" id="headingFive" type="button" data-toggle="collapse"
                                data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                <p class="mb-0"><span class="fw-700">05.</span> ¿Cuáles son los tipos de UPS? </p>
                            </button>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
                                        vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                                        no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row py-5" data-aos="fade-up" data-aos-duration="600">
                <div class="col-12 text-center">
                    <h4 class="h3 fw-300 mb-3">Si tenés más dudas, consultanos</h4>
                    <a href="" class="btn btn-primary mx-auto">Consultar</a>
                </div>
            </div>
        </div>
    </section>
    <section class="text-block">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-9 text-center text-white" data-aos="fade-right" data-aos-duration="600">
                    <h4 class="h2 fw-300">Energías del futuro</h4>
                    <div class="divider my-4 mx-auto"></div>
                    <p class="h3 fw-300">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                        took a galley of type.</p>

                </div>
            </div>
        </div>
    </section>
    <?php include '_footer.php'; ?>
    <?php include '_scripts.php'; ?>
</body>

</html>