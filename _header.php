<header>
    <div class="container-fluid px-0">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <nav class="navbar navbar-expand-lg navbar-light bsnav bsnav-light bsnav-sticky bsnav-sticky-slide">
                    <a class="navbar-brand" href="index.php">
                        <img src="images/logo.svg" class="header-logo" alt="">
                    </a>
                    <button class="navbar-toggler toggler-spring border-0"><span
                            class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbar">
                        <ul class="navbar-nav navbar-mobile">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Inicio</a>
                            </li>
                            <li class="nav-item open-submenu">
                                <a class="nav-link" href="soluciones.php">Soluciones</a>
                                <ul class="submenu-bottom">
                                    <li>
                                        <a href="">Sistemas Backups</a>
                                    </li>
                                    <li>
                                        <a href="">Vehículos Eléctricos</a>
                                    </li>
                                    <li>
                                        <a href="">Equipos de Riego</a>
                                    </li>
                                    <li>
                                        <a href="">Equipos de Energía Solar (off grid)
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">Otros</a>
                                    </li>
                                </ul>
                                <ul class="submenu">
                                    <li>
                                        <a href="">Sistemas Backups</a>
                                    </li>
                                    <li>
                                        <a href="">Vehículos Eléctricos</a>
                                    </li>
                                    <li>
                                        <a href="">Equipos de Riego</a>
                                    </li>
                                    <li>
                                        <a href="">Equipos de Energía Solar (off grid)
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">Otros</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="quienes-somos.php">Quienes somos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contacto.php">Contacto</a>
                            </li>
                            <li class="">
                                <div class="dropdown">
                                    <a class="dropdown-toggle nav-link text-capitalize" href="#" role="button"
                                        id="dropdownMenuLink" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-globe" aria-hidden="true"></i> English
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="">Spanish</a>
                                        <a class="dropdown-item" href="">English</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                </nav>

            </div>
        </div>
    </div>
</header>