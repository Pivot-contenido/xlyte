<!DOCTYPE html>
<html lang="en">
<?php include '_head.php'; ?>

<body>
    <?php include '_header.php'; ?>
    <div class="wrapper">
        <section class="section-title contacto">
            <div class="section-title-overlay"></div>
            <div class="container">
                <div class="row" data-aos="fade-right" data-aos-duration="400">
                    <div class="col-12 text-center">
                        <h2 class="h2 fw-300 text-white">Contacto</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="contact py-5">
            <div class="container-fluid pr-lg-0">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6 col-lg-5 col-xl-4 offset-xl-1 my-4">
                        <h3 class="h3 fw-300 primary">Formulario de contacto</h3>
                        <div class="left-divider mt-2 mb-3"></div>
                        <p class="fw-300 h5 gray">
                            Por consultas o sugerencias complete el siguiente formulario y nuestro equipo
                            de atención al cliente se pondrá en contacto a la brevedad.</span></strong>
                        </p>
                        <form class="mt-4">
                            <div class="form-row">
                                <div class="col my-2">
                                    <input type="text" class="form-control" placeholder="Nombre">
                                </div>
                                <div class="col my-2">
                                    <input type="text" class="form-control" placeholder="Apellido">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col my-2">
                                    <input type="text" class="form-control" placeholder="Teléfono">
                                </div>
                                <div class="col my-2">
                                    <input type="text" class="form-control" placeholder="Ciudad">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col my-2">
                                    <select id="inputState" class="form-control">
                                        <option selected>Producto de interés</option>
                                        <option>...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col my-2">
                                    <div class="form-group">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                                            placeholder="Mensaje"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary d-flex mx-auto">Enviar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 ml-auto position-relative my-4">
                        <div class="contact-block light-bg text-center">
                            <h3 class="h3 fw-300 gray">Contactanos</h3>
                            <div class="contact-icon-deco my-3"><i class="fa fa-phone"></i></div>
                            <a href="tel:+543571501062">+54 (3571) 501062 / 63</a>
                            <div class="contact-icon-deco my-3"><i class="fa fa-envelope"></i></div>
                            <a href="mailto:solar@baratec.com">solar@baratec.com</a>
                            <div class="contact-icon-deco my-3"><i class="fa fa-map-marker"></i></div>
                            <p>Córdoba 112, Almafuerte,<br>
                                Córdoba, Argentina</p>

                            <div class="row mt-5 social-media-container justify-content-center">
                                <div class="col-auto pr-1">
                                    <a href="" target="_blank">
                                        <div class="social-media">
                                            <i class="fab fa-instagram"></i>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-auto px-1">
                                    <a href="" target="_blank">
                                        <div class="social-media"> <i class="fab fa-facebook-f"></i></div>
                                    </a>
                                </div>
                                <div class="col-auto px-1">
                                    <a href="" target="_blank">
                                        <div class="social-media"> <i class="fab fa-youtube"></i></div>
                                    </a>
                                </div>
                                <div class="col-auto px-1">
                                    <a href="" target="_blank">
                                        <div class="social-media">
                                            <i class="fab fa-linkedin-in"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <img src="images/contacto-deco.jpg" class="w-100 contact-deco" alt="">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include '_footer.php'; ?>
    <?php include '_scripts.php'; ?>
</body>

</html>