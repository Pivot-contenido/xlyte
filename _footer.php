        <!-- FLOAT BTN WSP -->
        <a href="" class="wsp-float" target="_blank">
            <i class="fa fa-whatsapp"></i>
        </a>
        <!-- FOOTER-->
        <footer class="">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-3 my-3 d-none d-lg-block">
                        <img src="images/blue-logo.svg" class="footer-logo" alt="Sabonis">
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 my-3 d-none d-md-block">
                        <h6>Soluciones</h6>
                        <ul>
                            <li>
                                <a href="">Sistemas de backup</a>
                            </li>
                            <li>
                                <a href="">Vehículos eléctricos</a>
                            </li>
                            <li>
                                <a href="">Equipos de riego</a>
                            </li>
                            <li>
                                <a href="">Equipos de energía solar (off grid)</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 my-3 d-none d-md-block">
                        <h6>Quiénes somos</h6>
                        <ul>
                            <li>
                                <a href="">Nuestra historia</a>
                            </li>
                            <li>
                                <a href="">Misión, Visión, Valores</a>
                            </li>
                            <li>
                                <a href="">Cultura colaborativa</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-4 col-lg-3 my-3 mobile-center">
                        <h6>Contacto</h6>
                        <ul>
                            <li>
                                <a href="">Formulario de contacto</a>
                            </li>
                            <li>
                                <a href="">Solicitar presupuesto</a>
                            </li>
                        </ul>
                        <div class="row mt-4 social-media-container mobile-center">
                            <div class="col-auto pr-1">
                                <a href="" target="_blank">
                                    <div class="social-media">
                                        <i class="fab fa-instagram"></i>
                                    </div>
                                </a>
                            </div>
                            <div class="col-auto px-1">
                                <a href="" target="_blank">
                                    <div class="social-media"> <i class="fab fa-facebook-f"></i></div>
                                </a>
                            </div>
                            <div class="col-auto px-1">
                                <a href="" target="_blank">
                                    <div class="social-media"> <i class="fab fa-youtube"></i></div>
                                </a>
                            </div>
                            <div class="col-auto px-1">
                                <a href="" target="_blank">
                                    <div class="social-media">
                                        <i class="fab fa-linkedin-in"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy-end py-2 mt-5">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="text-center d-flex justify-content-between align-items-center">
                                <p class="mb-0 fw-400">
                                Baratec ©2022 - Todos los derechos reservados
                                </p>
                                <a href="http://www.web-media.com.ar/" target="blank"><img src="images/webmedia.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>