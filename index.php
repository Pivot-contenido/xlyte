<!DOCTYPE html>
<html lang="en">
<?php include '_head.php'; ?>

<body>
    <?php include '_header.php'; ?>
    <!-- MAIN SECTION -->
    <?php include '_hero.php'; ?>
    <section class="solutions py-5">
        <div class="container">
            <div class="row justify-content-center" data-aos="fade-up" data-aos-duration="600">
                <div class="col-12 text-center my-4">
                    <h4 class="h2 fw-300">Nuestras soluciones</h4>
                </div>
                <div class="col-12 my-3">
                    <div class="owl-carousel owl-theme-default" id="owl-home">
                        <a href="">
                            <div class="carousel-block">
                                <img src="images/carousel1.jpg" class="carousel-block-image" alt="">
                                <div class="carousel-block-text">
                                    <p class="h4 title">
                                        Sistemas de Backup
                                    </p>
                                    <p class="description">Sistemas para aplicaciones especiales como, conversión de + vehículos eléctricos,
                                        carros de golf, autoelevadores eléctricos, etc.</p>
                                    <div class="btn btn-primary">más información</div>
                                </div>
                            </div>
                        </a>
                        <a href="">
                            <div class="carousel-block">
                                <img src="images/carousel2.jpg" class="carousel-block-image" alt="">
                                <div class="carousel-block-text">
                                    <p class="h4 title">
                                        Vehículos eléctricos
                                    </p>
                                    <p class="description">Aplicaciones especiales como, conversión de + vehículos eléctricos, carros de
                                        golf, autoelevadores eléctricos, etc.</p>
                                    <div class="btn btn-primary">más información</div>
                                </div>
                            </div>
                        </a>
                        <a href="">
                            <div class="carousel-block">
                                <img src="images/carousel3.jpg" class="carousel-block-image" alt="">
                                <div class="carousel-block-text">
                                    <p class="h4 title">
                                        Equipos de energía solar
                                        sistema off grid
                                    </p>
                                    <p class="description">Sistemas para aplicaciones especiales como, conversión de + vehículos eléctricos,
                                        carros de golf, autoelevadores eléctricos, etc.</p>
                                    <div class="btn btn-primary">más información</div>
                                </div>
                            </div>
                        </a>
                        <a href="">
                            <div class="carousel-block">
                                <img src="images/carousel4.jpg" class="carousel-block-image" alt="">
                                <div class="carousel-block-text">
                                    <p class="h4 title">
                                        Equipos de Riego
                                    </p>
                                    <p class="description">Aplicaciones especiales como, conversión de + vehículos eléctricos, carros de
                                        golf, autoelevadores eléctricos, etc.</p>
                                    <div class="btn btn-primary">más información</div>
                                </div>
                            </div>
                        </a>
                        <a href="">
                            <div class="carousel-block">
                                <img src="images/carousel4.jpg" class="carousel-block-image" alt="">
                                <div class="carousel-block-text">
                                    <p class="h4">
                                        Equipos de Riego
                                    </p>
                                    <p class="description">Aplicaciones especiales como, conversión de + vehículos eléctricos, carros de
                                        golf, autoelevadores eléctricos, etc.</p>
                                    <div class="btn btn-primary">más información</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="text-block">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-9 text-center text-white" data-aos="fade-right" data-aos-duration="600">
                    <h4 class="h2 fw-300">Energías del futuro</h4>
                    <div class="divider my-4 mx-auto"></div>
                    <p class="h3 fw-300">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                        took a galley of type.</p>

                </div>
            </div>
        </div>
    </section>
    <?php include '_footer.php'; ?>
    <?php include '_scripts.php'; ?>
</body>

</html>